<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->url = env("URL_BACKEND");
    }
    public function indexLogin()
    {
        return view("app.auth.auth-login-basic");
    }

    public function indexRegister()
    {
        return view("app.auth.auth-register-basic");
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        try {
            $response = json_decode(Http::post("{$this->url}/api/user/login",[
                "email" => $email,
                "password" => $password
            ]));
            if($response->status) {
                session(["user" => $response->data]);
                return redirect()->route("dashboard");
            } else {
                return redirect()->back()->with("errors", "{$response->message}: email atau password salah!");
            }
            
        } catch (\Throwable $th) {
            $message = $th->getMessage();
        }
        return redirect()->back()->with("errors","System backend bermasalah: {$message}");
    }
}
