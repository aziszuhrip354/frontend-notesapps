<!DOCTYPE html>

<!-- =========================================================
* Sneat - Bootstrap 5 HTML Admin Template - Pro | v1.0.0
==============================================================

* Product Page: https://themeselection.com/products/sneat-bootstrap-html-admin-template/
* Created by: ThemeSelection
* License: You must have a valid license purchased in order to legally use the theme for your project.
* Copyright ThemeSelection (https://themeselection.com)

=========================================================
 -->
<!-- beautify ignore:start -->
<html
  lang="en"
  class="light-style customizer-hide"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../assets/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title>Login Pages | Generus Apps</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="../assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="../assets/vendor/css/pages/page-auth.css" />
    <!-- Helpers -->
    <script src="../assets/vendor/js/helpers.js"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="../assets/js/config.js"></script>
    <style>
      input {
        border: 2px solid #696cff!important
      }

      .input-group-text {
          display: flex;
          align-items: center;
          padding: 0.4375rem 0.875rem;
          font-size: 0.9375rem;
          font-weight: 400;
          line-height: 1.53;
          color: rgb(65, 67, 137);
          text-align: center;
          white-space: nowrap;
          background-color: #bfc0f8;
          border: 1px solid #bfc0f8;
          border-radius: 0.375rem;
      }

      @media only screen and (max-width: 767px) {
          .container-form {
            flex-direction: column;
            align-items: center;
            justify-content: center
          }
          .container-form, .container-icon {
            width: 100%!important;
            height: 100%;
          }

          .container-icon img {
            width: 40%!important;
          }

        }
        .container-icon {
          flex-direction:column;
        }

        .bg-container {
          background-color: #244555;
        }

        .scroll-container {
          height: 600px;
          overflow-y: scroll
      }
      
      .hollowLoader {
        width: 3em;
        height: 3em;
        -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
                animation: loaderAnim 1.25s infinite ease-in-out;
        outline: 1px solid transparent;
      }
      .hollowLoader .largeBox {
        height: 3em;
        width: 3em;
        background-color: #6a6cfe52;
        outline: 1px solid transparent;
      }
      .hollowLoader .smallBox {
        height: 3em;
        width: 3em;
        background-color: #6a6cfe;
        z-index: 1;
        outline: 1px solid transparent;
        -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
                animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
      }

      @-webkit-keyframes smallBoxAnim {
        0% {
          transform: scale(0.2);
        }
        100% {
          transform: scale(0.75);
        }
      }

      @keyframes smallBoxAnim {
        0% {
          transform: scale(0.2);
        }
        100% {
          transform: scale(0.75);
        }
      }
      @-webkit-keyframes loaderAnim {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(90deg);
        }
      }
      @keyframes loaderAnim {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(90deg);
        }
      }

      .main-container-loading {
          position: fixed;
          top: 0;
          width: 100%;
          height: 100%;
          background-color: #3232322b;
          z-index: 9999999999999999999999;
          justify-content: center;
          align-items: center;
          display: flex;
      }
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(90deg);
        }
      }

      .container-loading {
          position: absolute;
          width: 100%;
          height: 100%;
          background-color: #3232322b;
          z-index: 99;
          justify-content: center;
          align-items: center;
          display: flex;
      }
    </style>
  </head>

  <body>
    <!-- Content -->
    <div class="vh-100 d-flex w-100 container-form">
      <div class="d-flex justify-content-center align-items-center w-50 col-md-6 container-form">
        <div class="authentication-inner">
          <!-- Register -->
          <div class="">
            <div class="card-body">
              <!-- Logo -->
              
              <!-- /Logo -->
              <h4 class="mb-2 text-center">Welcome to Notes Apps! 👋</h4>
              <p class="mb-4 text-center">Silahkan login yuk!</p>

              <form id="formAuthentication" class="mb-3" action="{{route('auth.login.post')}}" method="POST">
                <div class="alert alert-danger tostr" role="alert" id="message" style="display: none">
                  {{session("errors")}}
                </div>
                @csrf
                <div class="mb-3">
                  <label for="email" class="form-label">Email</label>
                  <input
                    type="text"
                    required
                    class="form-control"
                    id="email"
                    name="email"
                    value="{{old("email")}}"
                    placeholder="Enter your email or username"
                    autofocus
                  />
                </div>
                <div class="mb-3 form-password-toggle">
                  <div class="d-flex justify-content-between">
                    <label class="form-label" for="password">Password</label>
                    {{-- <a href="auth-forgot-password-basic.html">
                      <small>Lupa Password?</small>
                    </a> --}}
                  </div>
                  <div class="input-group input-group-merge">
                    <input
                      type="password"
                      required
                      id="password"
                      class="form-control"
                      name="password"
                      placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                      aria-describedby="password"
                    />
                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                  </div>
                </div>
                <div class="mb-3">
                  <button class="btn btn-primary d-grid w-100" type="submit">Masuk</button>
                </div>
              </form>

              <p class="text-center">
                <span>Belum punya akun?</span>
                <a href="{{route('auth.register')}}">
                  <span>Buat akun</span>
                </a>
              </p>
            </div>
          </div>
          <!-- /Register -->
        </div>
      </div>
      <div class="bg-container w-50 d-flex justify-content-center align-items-center col-md-6 container-icon ">
        <div class="app-brand justify-content-center">
          <a href="#" class="text-center fw-bold">
            <h1 class="text-white">Notes-Apps</h1>
            <p class="text-white text-small">Aplikasi untuk memanajemen catatan pribadi Anda.</p>
          </a>
        </div>
        <img class="w-50" src="{{asset('post-it.png')}}" alt="icon-notes">
      </div>
    </div>

     <!-- Main Container loading -->
     <div class="main-container-loading" id="main-container-loading">
      <div class="">
          <div class="mb-4 fw-bold text-primary">
              <span>LOADING...</span>
          </div>
          <div class="" style="margin-left: 15px;transform: scale(1.5);">
              <div class="hollowLoader">
                <div class="largeBox">
                  <div class="smallBox"></div>
                </div>
              </div>
          </div>
      </div>
    </div>

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="../js/main.js"></script>
    <script src="../js/swal.js"></script>
    <script src="../assets/vendor/libs/jquery/jquery.js"></script>
    <script src="../assets/vendor/libs/popper/popper.js"></script>
    <script src="../assets/vendor/js/bootstrap.js"></script>
    <script src="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

    <script src="../assets/vendor/js/menu.js"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="../assets/js/main.js"></script>

    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <script src="{{asset('/assets/vendor/libs/jquery/jquery.js')}}"></script>

    <script>
      $.ajaxSetup({
          'headers': {
              'X-CSRF-TOKEN': '{{csrf_token()}}'
          }
      })

      var $loading = $('#main-container-loading').hide();
          $(document)
              .ajaxStart(function () {
                  $loading.show();
              })
              .ajaxStop(function () {
                  $loading.hide();
          });
      @if(session("errors")) 
          $(".tostr").delay(500).fadeIn();
          $(".tostr").delay(3000).fadeOut();
      @endif
    </script>
  </body>
</html>
