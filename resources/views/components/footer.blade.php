<footer class="content-footer footer bg-footer-theme">
    <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
      <div class="mb-2 mb-md-0 text-center">
        Hak Cipta Notes-Apps ©
        <script>
          document.write(new Date().getFullYear());
        </script>
        | made with ❤️ by
        <a href="https://www.linkedin.com/in/azis-zuhri-pratomo/" target="_blank" class="footer-link fw-bolder">Azis Zuhri Pratomo</a>
      </div>
    </div>
  </footer>