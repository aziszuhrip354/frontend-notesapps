@extends('template.master-with-sidebar')

@section("title")
 {{$title->type_todolist == "limited" ? "TODOLIST" : "CATATAN"}} {{$title->title}}
@endsection

@section("css")
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<style>
    .font-07 {
        font-size: 0.7rem;
    }

    #container-events {
        display: flex;
        overflow-x: hidden
    }

  .users {
    margin-bottom: 3px;
    border-radius: 10px;
    flex: 0 0 100%;
 }

 .scroll-container {
     height: 600px;
     overflow-y: scroll
 }
 
 .hollowLoader {
  width: 3em;
  height: 3em;
  -webkit-animation: loaderAnim 1.25s infinite ease-in-out;
          animation: loaderAnim 1.25s infinite ease-in-out;
  outline: 1px solid transparent;
}
.hollowLoader .largeBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe52;
  outline: 1px solid transparent;
}
.hollowLoader .smallBox {
  height: 3em;
  width: 3em;
  background-color: #6a6cfe;
  z-index: 1;
  outline: 1px solid transparent;
  -webkit-animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
          animation: smallBoxAnim 1.25s alternate infinite ease-in-out;
}

@-webkit-keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}

@keyframes smallBoxAnim {
  0% {
    transform: scale(0.2);
  }
  100% {
    transform: scale(0.75);
  }
}
@-webkit-keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}
@keyframes loaderAnim {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(90deg);
  }
}

.container-loading {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: #3232322b;
    z-index: 99;
    justify-content: center;
    align-items: center;
    display: flex;
}

.btnAddEvent {
  position: fixed;
    z-index: 999;
    bottom: 4rem;
    right: 2rem;
}

.btnAddEvent button {
  padding: 1rem 1rem;
    border-radius: 2.5rem;
    border: none;
    /* box-shadow: 0 0 7px 4px #a1a2ce; */
}

.swal2-container {
  z-index: 9999999999!important;
}

.label-danger {
  width: fit-content!important;
  padding: 0.2rem 0.3rem;
  background-color: #ff3e1d26;
  border-radius: 0.2rem;
  font-size: 0.7rem;
}

.label-warning {
  width: fit-content!important;
  padding: 0.3rem;
  background-color: #ffab0030;
  border-radius: 0.2rem;
  font-size: 0.7rem;
}


.label-success {
  width: fit-content!important;
  padding: 0.3rem;
  background-color: #71dd3733;
  border-radius: 0.2rem;
  font-size: 0.7rem;
}

#btnDelete {
  position: absolute;
    right: -5px;
    top: -5px;
    background-color: #fff;
    border-radius: 0.5rem;
    opacity: 1;
    padding: 0.635rem;
    box-shadow: 0 0.125rem 0.25rem rgb(161 172 184 / 40%);
}



</style>
@endsection


@section("content")
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Buat {{$title->type_todolist == "limited" ? "Todolist" : "Catatan"}}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="formEvent">
            <div class="mb-3">
              <label for="title" class="form-label">Nama {{$title->type_todolist == "limited" ? "todolist" : "catatan"}}</label>
              @if($title->type_todolist == "limited")
                <input type="text" class="form-control" id="todolist_name" name="todolist_name" placeholder="masukan nama {{$title->type_todolist == "limited" ? "todolist" : "catatan"}}..">
              @else
                <textarea class="form-control" cols="30" rows="10" id="todolist_name" name="todolist_name" placeholder="masukan nama  {{$title->type_todolist == "limited" ? "todolist" : "catatan"}}.."></textarea>
              @endif
            </div>
            @if($title->type_todolist == "limited")
              <div class="mb-3">
                <div class="row">
                  <div class="col">
                    <label for="exampleInputEmail1" class="form-label w-100">DEADLINE</label>
                    <input id="deadline" class="form-control w-100" name="deadline" width="100%" />
                  </div>
                </div>
              </div>
            @endif
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="createNewTodoList();">Simpan</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModal2Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModal2Label">Edit Judul Catatan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="formEditTodolist">
            <input type="hidden" name="todo_id">
            <div class="mb-3">
              <label for="titleEdit" class="form-label">Nama Catatan</label>
              <textarea class="form-control" id="catatan" cols="30" rows="10" name="catatan" placeholder="masukan nama  {{$title->type_todolist == "limited" ? "todolist" : "catatan"}}.."></textarea>
            </div>
            @if($title->type_todolist == "limited")
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label w-100 text-center mb-2 fw-bold py-1">Rentang Waktu</label>
                <div class="row">
                  <div class="col">
                    <label for="exampleInputEmail1" class="form-label text-center w-100">Jam Mulai</label>
                    <input id="timepicker" class="form-control w-100" name="startAt" width="100%" />
                  </div>
                </div>
              </div>
            @endif
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="editTodolist();">Simpan</button>
        </div>
      </div>
    </div>
  </div>

    <div class="btnAddEvent">
      <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
      </button>
    </div>
    <!--/ Basic Bootstrap Table -->
    <div class="card">
        <form class="mx-3 mt-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">TAMPILKAN DATA</label>
                      <select name="total_users" id="total_users" onchange="filterTotalData(this)" class="form-control">
                          <option value="10">10 Data</option>
                          <option value="15">15 Data</option>
                          <option value="20">30 Data</option>
                          <option value="30">Semua Data</option>
                      </select>
                    </div>
                </div>
                @if($title->type_todolist == "limited")
                <div class="col-md-2">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label text-center w-100">STATUS</label>
                    <select class="form-control" id="optionNotesFilter" name="type_todolist_filter" onchange="filterStatus(this)">
                      <option class="text-center w-100" value="all">SEMUA</option>
                      <option class="text-center w-100" value="false">BELUM SELESAI</option>
                      <option class="text-center w-100" value="true">SELESAI</option>
                    </select>
                  </div>
              </div>
                @endif
                <div class="{{$title->type_todolist == "limited" ? "col-md-6" : "col-md-8"}}">
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Cari Nama {{$title->type_todolist == "limited" ? "Todolist" : "Catatan"}}</label>
                      <input type="text" class="form-control" id="searching_user" onkeyup="searchTodolistName(this);" placeholder="cari nama judul catatan...">
                    </div>
                </div>
            </div>
        </form>
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
        <hr class="m-0 shadow">
          <div class="scroll-container" style="position: relative;">
              <div class="container-loading" id="container-loading">
                  <div class="">
                      <div class="mb-4 fw-bold text-primary">
                          <span>LOADING...</span>
                      </div>
                      <div class="" style="margin-left: 15px;transform: scale(1.5);">
                          <div class="hollowLoader">
                            <div class="largeBox">
                              <div class="smallBox"></div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          <div class="" id="container-events"></div>

@endsection

@section("js")
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
{{-- <script src="{{asset("js/qrcode.js")}}"></script> --}}
<script>
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
   $('#deadline').datetimepicker({
      minDate: today,
      format: 'dd mmmm yyyy HH:MM',
      uiLibrary: 'bootstrap5',
      footer: true, modal: true
    });
    $('#timepicker2').datetimepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });

    $('#timepicker3').datetimepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });
    $('#timepicker4').datetimepicker({
            // minDate: today,
            uiLibrary: 'bootstrap5'
    });
    var $loading = $("#container-loading").hide();
    let search = null;
    let totalData = 10;
    let statusTodolist = "all"
    const getTodolist = async (endpoint) => {
           $loading.show();
           await fetch(endpoint+`?sort=desc&total_data=${totalData}&search=${search ?? ''}&statusTodolist=${statusTodolist}`)
                .then((response) => response.json())
                .then((data) => {
                    let responseData = data.data;
                    $("#container-events").html("");
                    try {
                        let template = ``;
                        responseData.forEach(val => {
                          let btn = '';
                          if(val.status == false) {
                            btn = `<div class='row my-3'>
                                          <div class='col'>
                                            <button class='btn btn-success w-100' onclick="changeStatus('${val._id}')">
                                              <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>
                                            </button>
                                          </div>
                                        </div>`
                          }
                            @if($title->type_todolist == "limited")
                              template += `<div class="users">
                                                  <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                      <button type="button" class="btn-close" id="btnDelete" onclick="deleteTitle('${val._id}','${val.description}')" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="mb-1 fw-bold text-uppercase">Nama {{$title->type_todolist == "limited" ? "TODOLIST" : "CATATAN"}} :</div>
                                                        <span class="mb-1 text-primary fw-bold">${val.description}</span>
                                                        <div class="mb-1 fw-bold text-uppercase">DEADLINE :</div>
                                                        <span class="mb-1 text-uppercase ${val.type_todolist == "limited" ? "text-danger label-danger " : "text-warning label-warning"} fw-bold">${val.deadline}</span>
                                                        <div class="mb-1 fw-bold text-uppercase">STATUS :</div>
                                                        <span class="mb-1 text-uppercase ${val.status == false ? "text-danger label-danger " : "text-success label-success"} fw-bold">${val.status == false ? "BELUM SELESAI" : "SELESAI"}</span>
                                                        ${btn}
                                                    </div>
                                            </div>`
                            @else
                               template += `<div class="users">
                                                  <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                      <button type="button" class="btn-close" id="btnDelete" onclick="deleteTitle('${val._id}','${val.description}')" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        <div class="mb-1 fw-bold text-uppercase">Nama {{$title->type_todolist == "limited" ? "TODOLIST" : "CATATAN"}} :</div>
                                                        <span class="mb-1 text-primary fw-bold">${val.description}</span>
                                                        <div class='row my-3'>
                                                          <div class='col'>
                                                            <button class='btn btn-warning w-100' onclick="openModalEdit('${val._id}','${btoa(val.description)}');">
                                                              <svg viewBox="0 0 24 24" width="24" height="24" stroke="white" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                                            </button>
                                                          </div>
                                                        </div>
                                                    </div>
                                            </div>`           
                            @endif 
                        });
                        $("#container-events").append(template)
                        
                    } catch (error) {
                        let templateErr = `<div class="users">
                                                <div class="mx-2 my-4 box-shadow">
                                                    <div class="card shadow p-3 py-2 mx-2">
                                                        <div class="mb-1 fw-bold font-italic text-center">Data tidak ditemukan</div>           
                                                    </div>
                                            </div>`
                        $("#container-events").append(templateErr)
                    }

          });
        setTimeout(() => {
          $loading.hide();  
        }, 500);
  }

  getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');

  const filterTotalData = (self) => {
     totalData = $(self).val();
    getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
  }

  const searchTodolistName = (self) => {
      search = $(self).val();
      getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
  }

  const filterStatus = (self) => {
    statusTodolist = $(self).val();
    getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
  }

  const createNewTodoList = () => {
    let description = $("[name='todolist_name']").val();
    let deadline = $("[name='deadline']").val();
    let title_id = '{{$titleId}}';
    $.ajax({
          url: '{{env('URL_BACKEND')}}/api/todolist',
          dataType: "json",
          type: "Post",
          async: true,
          data: {
            deadline, description, deadline, title_id
          },
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              $("#exampleModal").modal("hide");
              $("#formEvent input").val("");
              getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: `${error.message}: ${error.error}`,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
    }); 
  }

  const editTodolist = (self) => {
    let description = $("#exampleModal2 [name='catatan']").val();
    let todo_id =  $("#exampleModal2 input[name='todo_id']").val();
    let title =  $("#exampleModal2 input[name='catatan']").val();
    let type_todolist = $("#exampleModal2 select[name='type_todolist_edit']").val();
    $.ajax({
          url: '{{env('URL_BACKEND')}}/api/todolist',
          dataType: "json",
          type: "PUT",
          async: true,
          data: {
            todo_id, description
          },
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              $("#exampleModal2").modal("hide");
              $("#formEditTodolist input").val("");
              getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
    }); 
  }

  const changeStatus = (todo_id) => {
    $.ajax({
          url: '{{env('URL_BACKEND')}}/api/todolist/change-status',
          dataType: "json",
          type: "PUT",
          async: true,
          data: {
            todo_id, 
          },
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              $("#exampleModal2").modal("hide");
              $("#formEditTodolist input").val("");
              getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
    }); 
  }

  const openModalEdit = (todo_id, todoName) => {
      $("#exampleModal2 input[name='todo_id']").val(todo_id);
      $("#exampleModal2 [name='catatan']").val(atob(todoName));
      $("#exampleModal2").modal("show");
  }

  const deleteTitle = (todo_id, titleName) => {
       Swal.fire({
        title: 'Pesan!',
        text: `Apakah Anda ingin menghapus ${titleName}?`,
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: "Tidak",
    }).then(val => {
      if(val.isConfirmed) {
        $.ajax({
          url: '{{env('URL_BACKEND')}}/api/todolist',
          dataType: "json",
          type: "DELETE",
          async: true,
          data: {todo_id},
          success: function (data) {
            Swal.fire({
                title: 'Pesan!',
                text: data.message,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
              getTodolist('{{env("URL_BACKEND")}}/api/todolist/all/{{$titleId}}');
          },
          error: function (xhr, exception) {
             let error = xhr.responseJSON;
              Swal.fire({
                title: 'Pesan!',
                text: error.message,
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
              });
          }
        }); 
      }
    });
  }

</script>

@endsection
