<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('auth.login');
});

Route::controller(AuthController::class)->group(function() {
    Route::prefix("auth")->group(function() {
        Route::get("login","indexLogin")->name("auth.login");
        Route::post("login","login")->name("auth.login.post");
        Route::get("register","indexRegister")->name("auth.register");
    });
});

Route::middleware("valid-user")->group(function() {
    Route::prefix("dashboard")->group(function() {
        Route::controller(DashboardController::class)->group(function() {
            Route::get("/", "dashboardIndex")->name("dashboard");
            Route::get("list-title-notes", "notesTitleIndex")->name("dashboard.notes.title.index");
            Route::get("notes/{title_id}", "todoListIndex")->name("dashboard.notes.index");
            Route::post("logout","logout")->name("logout");
        });
    });
});