<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(!session("user")) {
            return redirect()->route("auth.login")->with("errors","Silahkan login dahulu!");
        }
        return $next($request);
    }
}
