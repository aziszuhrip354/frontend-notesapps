
@extends('template.master-with-sidebar')

@section("title")
 DASHBOARD
@endsection

@section("content")
<div class="row">
    <div class="col-lg-12 mb-4 order-0">
      <div class="card">
        <div class="d-flex align-items-end row">
          <div class="col-sm-7">
            <div class="card-body">

            <h5 class="card-title text-primary">Hallo, kak {{user()->firstname." ".user()->lastname}}! 🎉</h5>
              <p class="mb-4">
                Selamat Datang Di NotesApps
              </p>

              {{-- <a href="javascript:;" class="btn btn-sm btn-outline-primary">Analisis</a> --}}
            </div>
          </div>
          <div class="col-sm-5 text-center text-sm-left">
            <div class="card-body pb-0 px-0 px-md-4">
              <img
                src="../assets/img/illustrations/girl-doing-yoga-light.png"
                height="140"
                alt="View Badge User"
                data-app-dark-img="illustrations/man-with-laptop-dark.png"
                data-app-light-img="illustrations/man-with-laptop-light.png"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-lg-12 mb-2 order-0">
        
      </div>
      <div class="col-lg-12 mb-4 order-0">
        <div class="card">
          <input type="text" id="clipboard_input" class="d-none">
            <div class="" id="container-channel">
            </div>
        </div>
      </div>
    <!--/ Total Revenue -->
    <div class="col-12">
      <div class="row">
        <div class="col-6 mb-4">
          <div class="card">
            <div class="card-body">
              <div class="card-title d-flex align-items-start justify-content-between">
                <div class="avatar flex-shrink-0">
                    <button class="btn btn-warning px-2 py-1 ">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="white" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z"></path><path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z"></path></svg>
                    </button>
                </div>
              </div>
              <span class="d-block mb-1">Total Catatan</span>
              <h3 class="card-title text-nowrap mb-2">{{$totalNotes}}</h3>
              {{-- <small class="text-danger fw-semibold"><i class="bx bx-down-arrow-alt"></i> -14.82%</small> --}}
            </div>
          </div>
        </div>
        <div class="col-6 mb-4">
          <div class="card">
            <div class="card-body">
              <div class="card-title d-flex align-items-start justify-content-between">
                <div class="avatar flex-shrink-0">
                    <button class="btn btn-primary px-2 py-1 ">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="white" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3.01" y2="6"></line><line x1="3" y1="12" x2="3.01" y2="12"></line><line x1="3" y1="18" x2="3.01" y2="18"></line></svg>
                    </button>
                </div>
              </div>
              <span class="fw-semibold d-block mb-1">Total Todolist</span>
              <h3 class="card-title mb-2">{{$totalTodoList}}</h3>
              {{-- <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +28.14%</small> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"  aria-labelledby="staticBackdropLabel" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="staticBackdropLabel">TAMBAH CHANNEL</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="">
              <div class="container-opsi-channel d-flex">
                  <a href="javascript:void(0);" class=" w-50 text-center m-1 text-primary bdr-primary fw-bold" id="create-btn" onclick="changeOptionChannel(this);">BARU</a>
                  <a href="javascript:void(0);" class=" w-50 text-center m-1 text-primary" id="join-btn" onclick="changeOptionChannel(this);">GABUNG</a>
              </div>
              <div class="my-2">
                <form class="">
                    <div class="mb-3" id="input-new-channel">
                      <label for="exampleInputPassword1" class="form-label" >Nama Channel</label>
                      <input type="text" class="form-control" onkeyup="toUpperCase(this);" id="exampleInputPassword1">
                    </div>
                    <div class="mb-3" id="input-code-channel" style="display: none">
                        <label class="form-label" for="phoneNumber">CODE CHANNEL</label>
                        <div class="input-group input-group-merge">
                          <span class="input-group-text fw-bold">#</span>
                          <input type="text" id="phoneNumber" name="phoneNumber" class="form-control" placeholder="XXXXXX">
                        </div>
                      </div>
                  </form>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="btn-add-channel" data-button="create" onclick="tambahChannel(this)">Buat</button>
        </div>
      </div>
    </div>
  </div>
  
@endsection