<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->endpoint = env("URL_BACKEND");
    }

    public function dashboardIndex()
    {   
        $userId= user()->_id;
        $getDataUser = json_decode(http::get("{$this->endpoint}/api/user/{$userId}"));
        $totalTodoList = $getDataUser->data->total_todolist;
        $totalNotes = $getDataUser->data->total_notes;
        $dataArr[] = "totalTodoList";
        $dataArr[] = "totalNotes";
        return view("app.dashboard-channel.index", compact($dataArr));
    }

    public function logout()
    {
        session()->forget("user");
        return redirect()->route('auth.login');
    }

    public function notesTitleIndex()
    {
        return view("app.dashboard-channel.notes-title");
    }

    public function todoListIndex($titleId)
    {
        $respponse = json_decode(Http::get($this->endpoint."/api/todolist-title/{$titleId}"));
        if($respponse->message != "success to get todolist title") {
            abort(404);
        }
        $title = $respponse->data;
        $dataArr[] = "titleId";
        $dataArr[] = "title";
        return view("app.dashboard-channel.notes",compact($dataArr));
    }
}
